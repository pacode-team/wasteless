import {List} from 'immutable';

const initialState = {
    counter: 0,
    shoppingListItems: List([
        {
            id: 0,
            name: 'Orange juice',
            daysLeft: 1,
            lifeSpan: 20,
            foodContainer: 'fridge',
            price: 3,
            quantity: 1,
            previousQuantity: 3,
            iconName: 'local-drink',
            iconPack: 'MaterialIcons',
        },
        {
            id: 7,
            name: 'Pomegranate juice',
            daysLeft: 1,
            lifeSpan: 20,
            foodContainer: 'fridge',
            price: 3,
            quantity: 4,
            iconName: 'local-drink',
            iconPack: 'MaterialIcons',
        }
    ]),
    foodItems: List([
        {
            id: 0,
            name: 'Orange juice',
            daysLeft: 1,
            lifeSpan: 20,
            foodContainer: 'fridge',
            price: 3,
            quantity: 1,
            iconName: 'local-drink',
            iconPack: 'MaterialIcons',
        },
        {
            id: 1,
            name: 'Lettuce',
            daysLeft: 3,
            lifeSpan: 7,
            foodContainer: 'fridge',
            price: 2,
            quantity: 2,
            iconName: 'food',
            iconPack: 'MaterialCommunityIcons',
        },
        {
            id: 2,
            name: 'Cherry',
            daysLeft: -2,
            lifeSpan: 7,
            foodContainer: 'fridge',
            price: 2,
            quantity: 3,
            iconName: 'food',
            iconPack: 'MaterialCommunityIcons',
        },
        {
            id: 3,
            name: 'Onion',
            daysLeft: 18,
            lifeSpan: 21,
            quantity: 1,
            foodContainer: 'fridge',
            price: 2,
            iconName: 'food',
            iconPack: 'MaterialCommunityIcons',
        },
        {
            id: 4,
            name: 'Bread',
            daysLeft: 1,
            lifeSpan: 2,
            foodContainer: 'fridge',
            price: 3,
            quantity: 1,
            iconName: 'food',
            iconPack: 'MaterialCommunityIcons',
        },
    ]),
    consumedFood: List([
        {
            id: 6,
            name: 'Beef meat',
            daysLeft: 1,
            lifeSpan: 20,
            foodContainer: 'freezer',
            price: 3,
            quantity: 1,
            iconName: 'cow',
            iconPack: 'MaterialCommunityIcons',
        },
    ]),
    wastedFood: List([
        {
            id: 5,
            name: 'Frozen strawberries',
            daysLeft: 300,
            lifeSpan: 400,
            foodContainer: 'freezer',
            price: 3,
            quantity: 1,
            iconName: 'food-apple',
            iconPack: 'MaterialCommunityIcons',
        },
    ]),
};


export function mainReducer(state = initialState, action) {
    switch (action.type) {
        case 'COUNTER':
            return {
                ...state,
                counter: action.payload,
            };
        case 'REMOVE_ITEM':

            let toBeRemoved = state.foodItems.findIndex(m => m.id === action.payload);

            return {
                ...state,
                foodItems: state.foodItems.delete(toBeRemoved),
                wastedFood: state.wastedFood.push(state.foodItems.get(toBeRemoved))
            };

        case 'CONSUME_ITEM':
            let toBeConsumed = state.foodItems.findIndex(m => m.id === action.payload);

            return {
                ...state,
                foodItems: state.foodItems.delete(toBeConsumed),
                consumedFood: state.consumedFood.push(state.foodItems.get(toBeConsumed)),
            };
        case "HANDLE_SHOPPING_ITEM":
            let foodItem = action.payload;
            let newFoodList = state.foodItems;
            let foodItemIndex = state.foodItems.findIndex((item) => item.id === foodItem.id);
            if (foodItemIndex === -1) {
                newFoodList = newFoodList.push(foodItem)
            } else {
                newFoodList = newFoodList.delete(foodItemIndex);
            }
            return {
                ...state,
                foodItems: newFoodList
            };
        case 'ADD_NEW_FOOD':

            let {payload} = action;

            return {
                ...state,
                foodItems: state.foodItems.push(payload),
                shoppingListItems: state.shoppingListItems.push(payload)
            };
        default:
            return state;
    }
}
