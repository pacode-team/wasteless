import React, {Component} from 'react';
import {Modal, TouchableHighlight} from 'react-native';
import {
    Body,
    Button,
    Container,
    Content,
    Form,
    Header,
    Input,
    Item,
    Label,
    Picker,
    Root,
    Text,
    Title,
    Toast
} from 'native-base';
import PropTypes from 'prop-types';
import * as theme from '../constants/theme'
import {Ionicons, MaterialIcons} from '@expo/vector-icons';
import * as constants from '../constants/constantValues';
import * as uuid from "uuid";

class AddNewFood extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedContainer: 0,
            foodName: undefined,
            foodPrice: undefined,
            foodQuantity: undefined,
            foodLifespan: undefined
        }
    }

    changeFoodContainer = (itemId) => {
        this.setState({
            selectedContainer: itemId
        });
    };

    addNewFood = () => {
        let {foodName, foodPrice, foodQuantity, foodLifespan, selectedContainer} = this.state;

        let selectedContainerObject = constants.foodContainers.find(foodContainer => foodContainer.id === selectedContainer);

        if (typeof (foodName) !== 'undefined' && foodName.length !== 0
            && typeof (foodPrice) !== "undefined"
            && typeof (foodQuantity) !== 'undefined'
            && typeof (selectedContainerObject) !== "undefined"
            && typeof (foodLifespan) !== 'undefined' && foodLifespan > 0) {

            let newFood = {
                id: uuid.v1(),
                name: foodName,
                daysLeft: foodLifespan,
                lifeSpan: foodLifespan,
                foodContainer: selectedContainerObject.name,
                iconName: "food",
                iconPack: "MaterialCommunityIcons",
                price: foodPrice,
                quantity: foodQuantity
            };

            this.props.handleSubmitFood(newFood);
        } else {
            Toast.show({
                text: "Provided data for new food is incorrect.",
                buttonText: "Okay",
                duration: 2000,
                type: "danger"
            });
        }

    };


    render() {

        return (

            <Modal
                animationType='slide'
                transparent={false}
                visible={this.props.modalVisible}
                onRequestClose={() => this.props.closeModal()}
            >
                <Root>
                    <Container style={theme.modalHeader}>
                        <Header style={theme.headerPage}>
                            <Body style={{flexDirection: 'row'}}>
                                <Button transparent onPress={()=> this.props.closeModal()}>
                                    <MaterialIcons name="chevron-left" color='white' size={32}/>
                                </Button>
                                <Title
                                    style={{color: theme.textWhite, marginLeft: 10, paddingTop: 10, paddingBottom: 10}}>Add
                                    new food</Title>
                            </Body>
                        </Header>
                        <Content scrollEnabled={false} style={theme.content}>
                            <Form>
                                <Item floatingLabel>
                                    <Label>Food name</Label>
                                    <Input onChangeText={(text) => this.setState({foodName: text})}
                                    />
                                </Item>
                                <Item floatingLabel>
                                    <Label>Food price</Label>
                                    <Input keyboardType="numeric"
                                           onChangeText={(text) => this.setState({foodPrice: text})}/>
                                </Item>
                                <Item floatingLabel>
                                    <Label>Food quantity</Label>
                                    <Input keyboardType="numeric"
                                           onChangeText={(text) => this.setState({foodQuantity: text})}/>
                                </Item>
                                <Item floatingLabel>
                                    <Label>Food lifespan</Label>
                                    <Input keyboardType="numeric"
                                           onChangeText={(text) => this.setState({foodLifespan: text})}
                                    />
                                </Item>
                                <Item last picker>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Ionicons name="ios-arrow-down"/>}
                                        style={{width: undefined, marginTop: 10}}
                                        placeholder="Select food container"
                                        placeholderStyle={{color: "#bfc6ea"}}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.selectedContainer}
                                        onValueChange={this.changeFoodContainer}
                                    >
                                        {constants.foodContainers.map(item => <Picker.Item label={item.name}
                                                                                           value={item.id}
                                                                                           key={item.id + ""}/>)}
                                    </Picker>

                                </Item>
                            </Form>
                            <TouchableHighlight style={theme.buttonFull} onPress={() => {
                                this.addNewFood()
                            }}>
                                <Text style={{color: theme.textWhite, textAlign: 'center'}}>Add new food</Text>
                            </TouchableHighlight>
                        </Content>
                    </Container>
                </Root>
            </Modal>
        )
    }
}

AddNewFood.propTypes = {
    handleSubmitFood: PropTypes.func.isRequired,
    modalVisible: PropTypes.bool.isRequired,
    closeModal: PropTypes.func.isRequired
};

export default AddNewFood;