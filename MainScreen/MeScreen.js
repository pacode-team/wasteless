import React, {Component} from 'react';
import {Body, Container, Content, Header, Left, List, ListItem, Right, Text, Title} from 'native-base';
import * as theme from "../constants/theme";
import {Entypo, FontAwesome, MaterialCommunityIcons} from '@expo/vector-icons';
import {connect} from "react-redux";

class MeScreen extends Component {

    calculateMoneySpent() {
        let args = arguments;
        let moneySpent = 0;
        for (let i = 0; i < args.length; i++) {
            args[i].forEach(item => moneySpent += parseInt(item.price + ''));
        }
        return moneySpent;
    }

    render() {

        let {foodItems, consumedFood, wastedFood} = this.props;
        let spentMoney = this.calculateMoneySpent(foodItems, consumedFood, wastedFood);

        return (
            <Container style={theme.fullscreen}>
                <Header style={theme.headerPage}>
                    <Body>
                        <Title style={{color: theme.textWhite}}>My stats</Title>
                    </Body>
                </Header>
                <Content scrollEnabled={false} style={theme.content}>

                    <List>
                        <ListItem avatar>
                            <Left>
                                <FontAwesome name={"money"} color={"green"} size={32}/>
                            </Left>
                            <Body>
                                <Text>Spent</Text>
                                <Text note>Money you have spent on food</Text>
                            </Body>
                            <Right>
                                <Text note style={{color: 'green', fontSize: 32}}>{spentMoney}$</Text>
                            </Right>
                        </ListItem>
                        <ListItem avatar>
                            <Left>
                                <MaterialCommunityIcons name={"silverware-fork-knife"} color={"green"} size={32}/>
                            </Left>
                            <Body>
                                <Text>Consumed</Text>
                                <Text note>Number of food you have consumed</Text>
                            </Body>
                            <Right>
                                <Text note style={{color: 'green', fontSize: 32}}>{consumedFood.size}</Text>
                            </Right>
                        </ListItem>
                        <ListItem avatar>
                            <Left>
                                <Entypo name={"trash"} color={theme.danger} size={32}/>
                            </Left>
                            <Body>
                                <Text>Wasted</Text>
                                <Text note>Number of food you have wasted</Text>
                            </Body>
                            <Right>
                                <Text note style={{color: theme.danger, fontSize: 32}}>{wastedFood.size}</Text>
                            </Right>
                        </ListItem>
                        <ListItem avatar>
                            <Left>
                                <MaterialCommunityIcons name={"food"} color={theme.primaryLight} size={32}/>
                            </Left>
                            <Body>
                                <Text>To consume</Text>
                                <Text note>Number of food you have to consume</Text>
                            </Body>
                            <Right>
                                <Text note style={{color: theme.primaryLight, fontSize: 32}}>{foodItems.size}</Text>
                            </Right>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        shoppingListItems: state.mainReducer.shoppingListItems,
        foodItems: state.mainReducer.foodItems,
        consumedFood: state.mainReducer.consumedFood,
        wastedFood: state.mainReducer.wastedFood
    }
};

export default connect(mapStateToProps, null)(MeScreen)