import React, {Component} from 'react';
import {ListView, Platform, Text, View} from 'react-native';
import {
    Badge,
    Body,
    Button,
    Container,
    Content,
    Header,
    Icon,
    List,
    ListItem,
    Root,
    Segment,
    Title,
    Toast
} from 'native-base';
import {connect} from 'react-redux';
import * as theme from '../constants/theme';
import * as Progress from 'react-native-progress';
import * as actionCreators from '../actionCreators';

class FoodScreenDisconnected extends Component {

    state = {activeTabIdx: 0};

    deleteRow(dataId, secId, rowId, rowMap) {
        rowMap[`${secId}${rowId}`].props.closeRow();
        Toast.show({
            duration: 2000,
            buttonText: "Okay",
            type: "danger",
            text: "You have wasted " + this.props.foodItems.find(food => food.id === dataId).name
        });

        this.props.removeItem(dataId);
    }

    consumeRow(dataId, secId, rowId, rowMap) {
        rowMap[`${secId}${rowId}`].props.closeRow();
        Toast.show({
            duration: 2000,
            buttonText: "Okay",
            type: "success",
            text: "You have consumed " + this.props.foodItems.find(item => item.id === dataId).name
        });
        this.props.consumeItem(dataId);
    }

    render() {

        let groupedByContainer = this.props.foodItems.groupBy(x => x.foodContainer);
        let containers = groupedByContainer.keySeq().toJS();
        let items = groupedByContainer.get(containers[this.state.activeTabIdx]).toJS();
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        return (
            <Root>
                <Container style={theme.fullscreen}>
                    <Header hasSegment style={theme.headerPage}>
                        <Body>
                            <Title style={{color: theme.textWhite}}>Wasteless: food saver</Title>
                        </Body>
                    </Header>
                    <Segment style={{backgroundColor: Platform.OS === 'android' ? theme.primaryColor : 'white'}}>
                        {
                            containers.map(
                                (m, idx) => <Button key={idx} active={idx === this.state.activeTabIdx} first={idx === 0}
                                                    last={idx === containers.length - 1} onPress={() => {
                                    this.setState({activeTabIdx: idx});
                                }}>
                                    <Text
                                        style={{
                                            textTransform: 'capitalize',
                                            padding: Platform.OS === 'android' ? 10 : 5,
                                            color: this.state.activeTabIdx === idx ? (Platform.OS === 'android' ? theme.primaryColor : 'white') : (Platform.OS === 'android' ? 'white' : theme.primaryColor),
                                        }}>
                                        {m}
                                    </Text>
                                </Button>,
                            )
                        }
                    </Segment>
                    <Content scrollEnabled={false} style={theme.content}>

                        <List
                            leftOpenValue={75}
                            rightOpenValue={-75}
                            dataSource={ds.cloneWithRows(items)}
                            renderRow={data =>
                                <ListItem>
                                    <Text> {data.name} </Text>
                                    {data.daysLeft > 0 &&
                                    <Progress.Bar progress={data.daysLeft / data.lifeSpan} borderWidth={2} height={8}
                                                  borderRadius={7}
                                                  style={{marginLeft: 10}}
                                                  color={theme.primaryLight} borderColor={theme.primaryLight}/>}

                                    <View style={{alignSelf: "flex-end", marginLeft: "auto"}}>
                                        {data.daysLeft > 0 && data.daysLeft < 3 &&
                                        <Badge warning style={{marginLeft: 10}}><Text
                                            style={theme.badge}>expiring soon!</Text></Badge>}

                                        {data.daysLeft <= 0 &&
                                        <Badge danger style={{marginLeft: 10}}><Text
                                            style={theme.badge}>expired
                                            :(</Text></Badge>}
                                    </View>
                                </ListItem>}
                            renderLeftHiddenRow={(data, secId, rowId, rowMap) =>
                                <Button success onPress={() => this.consumeRow(data.id, secId, rowId, rowMap)}>
                                    <Icon active name="nutrition"/>
                                </Button>}
                            renderRightHiddenRow={(data, secId, rowId, rowMap) =>
                                <Button full danger onPress={() => this.deleteRow(data.id, secId, rowId, rowMap)}>
                                    <Icon active name="trash"/>
                                </Button>}
                        />
                    </Content>
                </Container>
            </Root>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    consumeItem: (id) => dispatch(actionCreators.consumeItem(id)),
    removeItem: (id) => dispatch(actionCreators.removeItem(id)),
});
const mapStateToProps = state => {
    return {
        foodItems: state.mainReducer.foodItems,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FoodScreenDisconnected);