import React, {Component} from 'react';
import {FlatList, Text, View} from 'react-native';
import {connect} from "react-redux";
import * as actionCreators from '../actionCreators';
import {Badge, Body, CheckBox, Container, Content, Fab, Header, ListItem, Title} from 'native-base';
import {MaterialIcons} from '@expo/vector-icons';
import * as theme from "../constants/theme"
import AddNewFood from './AddNewFood';

class ShoppingListItem extends Component {

    render() {
        let {item, foodItems} = this.props;
        return (
            <ListItem>
                <CheckBox
                    checked={typeof (foodItems.find(foodItem => foodItem.id === item.id)) !== "undefined"}
                    onPress={() => this.props.onPress(item)}
                    color={theme.primaryLight}
                />
                <Text style={theme.listItem}>{item.name} <Text
                    style={{fontSize: 12, color: theme.textGrey}}>({item.foodContainer})</Text></Text>
                <View style={{alignSelf: 'flex-end', flexDirection: "row", marginLeft: "auto"}}>
                    <Badge style={{backgroundColor: theme.primaryLight}}><Text
                        style={theme.badge}>{item.quantity}</Text></Badge>
                    {typeof (item.previousQuantity) !== "undefined" &&
                    <Badge warning
                           style={theme.previousQuantity}><Text
                        style={theme.badge}>Previous: {item.previousQuantity}</Text></Badge>
                    }
                </View>

            </ListItem>
        );
    }

}


class ShoppingListScreen extends Component {

    pressCheckBox = (item) => {

        this.props.changeSelectedItems(item);
    };

    state = {
        activeModal: false
    };

    handleSubmitModal = (item) => {
        this.props.addNewFood(item);
        this.setState({activeModal: false});
    };

    _keyExtractor = (item) => item.id + "";

    render() {
        return (

            <Container style={theme.fullscreen}>
                <Header style={theme.headerPage}>
                    <Body>
                        <Title style={{color: theme.textWhite}}>AI Shopping List</Title>
                    </Body>
                </Header>

                <Content scrollEnabled={false} style={theme.content}>
                    <FlatList data={this.props.shoppingListItems.toArray()} keyExtractor={this._keyExtractor}
                              renderItem={({item}) => (
                                  <ShoppingListItem item={item}
                                                    foodItems={this.props.foodItems}
                                                    onPress={this.pressCheckBox}
                                  />
                              )}/>


                </Content>
                <AddNewFood modalVisible={this.state.activeModal} handleSubmitFood={this.handleSubmitModal}
                            closeModal={() => this.setState({activeModal: false})}/>
                <Fab onPress={() => this.setState({activeModal: true})} style={{backgroundColor: theme.secondaryColorLight}}>
                    <MaterialIcons name='add-circle'/>
                </Fab>

            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    changeCounter: (payload) => dispatch(actionCreators.changeCounter(payload)),
    changeSelectedItems: (item) => dispatch(actionCreators.selectDeselectShoppingItem(item)),
    addNewFood: (newFood) => dispatch(actionCreators.addNewFood(newFood))
});

const mapStateToProps = state => {
    return {
        shoppingListItems: state.mainReducer.shoppingListItems,
        foodItems: state.mainReducer.foodItems
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingListScreen)

