export const changeCounter = (payload) => {
    return {
        type: 'COUNTER',
        payload: payload,
    };
};

export const consumeItem = (itemId) => {
    return { type: 'CONSUME_ITEM', payload: itemId };
};
export const removeItem = (itemId) => {
    return { type: 'REMOVE_ITEM', payload: itemId };
};

export const selectDeselectShoppingItem = (payload) => {
    return {
        type: "HANDLE_SHOPPING_ITEM",
        payload
    }
};

export const addNewFood = (newFood) => {
    return {
        type: "ADD_NEW_FOOD",
        payload: newFood
    }
};