import * as reducers from "./reducers";
import {combineReducers, createStore} from "redux";

export default function configureStore() {
    let store = createStore(
        combineReducers({
            ...reducers
        })
    );
    return store;
}