import Constants from 'expo-constants';
import {Platform} from "react-native";

export const backgroundColor = '#0C94E8';
export const primaryColor = '#0D63FF';
export const primaryLight = '#0C94E8';
export const primaryLightExtreme = "#00E3FF";
export const secondaryColorLight = '#0CE8C5';
export const secondaryColor = '#0DFF94';
export const pageBackgroundColor = 'white';
export const textWhite = 'white';
export const textGrey = "#848484";
export const danger = Platform.OS === "android" ? "#d9534f" : "#d9534f";

export const paddingTop = Constants.statusBarHeight;

export const fullscreen = {
    flexGrow: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: backgroundColor
};

export const modalHeader = {
    flexGrow: 1,
    backgroundColor: backgroundColor
};

export const content = {
    backgroundColor: pageBackgroundColor
};

export const headerPage = {
    backgroundColor: primaryColor,
    color: textWhite
};

export const listItem = {
    paddingLeft: 10
};

export const badge = {
    paddingTop: Platform.OS === "android" ? 3 : 0,
    color: 'white'
};

export const previousQuantity = {
    marginLeft: 10,
};

export const buttonFull = {
    padding: 10,
    backgroundColor: primaryColor,
    margin: 20,
    flexGrow: 1,
};