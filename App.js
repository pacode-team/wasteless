import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {Font} from 'expo';
import {createAppContainer, createBottomTabNavigator} from 'react-navigation';
import FoodScreen from './MainScreen/FoodScreen';
import {Ionicons} from '@expo/vector-icons';
import MeScreen from './MainScreen/MeScreen';
import ShoppingListScreen from './MainScreen/ShoppingListScreen';
import {Provider} from 'react-redux';
import configureStore from './store';
import * as theme from './constants/theme'
import {Spinner} from 'native-base';

const store = configureStore();

export default class App extends Component {
    state = {
        isReady: false,
    };

    async componentWillMount() {
        await Font.loadAsync({
            Roboto: require('native-base/Fonts/Roboto.ttf'),
            Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
            Ionicons: require('native-base/Fonts/Ionicons.ttf'),
        });
        this.setState({isReady: true});
    }

    render() {

        if (!this.state.isReady) {
            return <View style={{
                paddingTop: theme.paddingTop,
            }}><Spinner color={theme.primaryColor}/></View>;
        }

        const AppContainer = createAppContainer(createBottomTabNavigator(
            {
                PaperScreen: ShoppingListScreen,
                FoodScreen: FoodScreen,
                MeScreen: MeScreen,
            },
            {
                defaultNavigationOptions: ({navigation}) => ({
                    tabBarLabel: ({focused, tintColor}) => {
                        const {routeName} = navigation.state;
                        let textValue;
                        switch (routeName) {
                            case 'PaperScreen':
                                textValue = "AI Shopping List";
                                break;
                            case 'FoodScreen':
                                textValue = "Food";
                                break;
                            default:
                                textValue = "Me";
                                break;
                        }

                        return <Text style={{color: tintColor, textAlign: 'center'}}>{textValue}</Text>
                    },
                    tabBarIcon: ({focused, horizontal, tintColor}) => {
                        const {routeName} = navigation.state;
                        let IconComponent = Ionicons;
                        let iconName;
                        switch (routeName) {
                            case 'PaperScreen':

                                iconName = "md-paper";
                                break;
                            case 'FoodScreen':
                                iconName = "md-nutrition";
                                break;
                            default:
                                iconName = "md-pie";
                                break;
                        }

                        return <IconComponent name={iconName} size={focused ? 32 : 30} color={tintColor}/>

                    }
                }),
                tabBarOptions: {
                    activeTintColor: theme.primaryColor,
                    inactiveTintColor: "grey",
                    style: {
                        height: 55
                    }

                },
            },
        ));
        return <Provider store={store}><AppContainer/></Provider>;
    }
}
